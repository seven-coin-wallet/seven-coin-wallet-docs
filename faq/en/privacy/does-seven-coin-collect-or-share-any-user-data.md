# Does Seven Coin collect or share any user data?

Users can opt-in to send data for use in analytics tools. This is entirely optional. The wallet is designed to ensure a high level of privacy for its users.