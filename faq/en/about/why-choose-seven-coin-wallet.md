# Why choose Seven Coin Wallet?

To be Seven Coin!

To unchain assets, go borderless and stay private.

Seven Coin is primarily an asset management tool built for those looking to decentralize capital, exercise genuine control over assets and grow wealth through borderless opportunities.

If the above rhymes with you, then Seven Coin is for you. We are here to focus on your unique needs and serve you in the best possible manner.

If you're looking for a plain way to store Bitcoin or some crypto, this wallet app might not be for you.