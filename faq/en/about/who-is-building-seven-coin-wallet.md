# Who is building Seven Coin?

Seven Coin started in 2022.

- The transition to crypto was a result of experiencing limited problems when dealing with traditional finance on a business as well as individual levels.

- The idea of building a decentralized asset storage and management instrument outside the reach of traditional finance was too hard to resist.