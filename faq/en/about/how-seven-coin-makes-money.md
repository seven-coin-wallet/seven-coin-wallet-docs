# How does Seven Coin make money?

Seven Coin is a free to use instrument that keeps all of its fundamental features free.

We are a self-funded group and not dependent on any funding from external entities. Our former ventures allow us to stay independent, creative and experimental, while building our preferred asset management instrument for the decentralized fintech.
