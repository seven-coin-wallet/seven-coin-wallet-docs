# What risks are there when using Seven Coin?

Seven Coin Wallet has no means to interfere with the user's funds or in any way restrict anyone from using the app. The wallet user is the only entity in control of the funds.

As a result of being non-custodial, private and decentralized, Seven Coin brings full power into the hands of the user.

With exclusive control over assets, users also get full responsibility. There is no custodian entity responsible for the safety of the funds, other than the users themselves. In case there are issues, there is no one to compensate for losses.

Key wallet features in Seven Coin do not depend on any centrally managed servers or any infrastructure managed by the Seven Coin team.

Potential risks include:

- Unexpected software bugs. It's potentially possible for the wallet to have undiscovered issues which may lead to unexpected behavior in the app.

- Poor security practices. Problems which may originate from users themselves as a result of poor security practices, like rooting the phone, not using a device PIN lock, keeping the phone OS outdated, installing apps from unauthorized sources, etc.

- Using an unofficial app. Always update or install the Seven Coin app by downloading it from one of the official resources.

- Incorrect Metadata. Incorrect metadata in the app itself i.e. outdated exchange rates, can lead to issues. Seven Coin relies on third party services to display current market rates for the given cryptocurrency. While rates are obtained from reputable 3rd party sources, Seven Coin can not guarantee they do not differ from global market rates. Therefore, when sending someone a large amount in cryptocurrency, it's safer to enter the payment amount in cryptocurrency (when entering the amount into the input form) rather than in its fiat equivalent.

- Users should never share wallet private keys (aka 12-24 word mnemonic phrase) with anyone, including the Seven Coin team. All potential problems with the wallet app can be addressed without the need for a wallet private key.