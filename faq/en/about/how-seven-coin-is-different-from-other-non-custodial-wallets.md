# How is Seven Coin different?

The Seven Coin Wallet app has been a work in progress since late 2022

It's the most sophisticated non-custodial wallet app to date that goes beyond non-custodial storage capabilities, and takes a comprehensive approach towards privacy, decentralization and usability.

- Built per security best practices.
- Decentralized blockchain communication.
- Supports unlimited number of multi-coin wallets.
- Decentralized Finance enabled wallet.
- Educates users on crypto and DeFi.
- Natively built for iOS/Android.

When using a non-custodial wallet like Seven Coin, the control over funds and access to them doesn't depend on any intermediary. The user has unconditional control over their funds.

Most wallets provided by cryptocurrency exchanges, including Binance, Coinbase, Gemini, Kraken, Poloniex, Bittrex are of a custodial type. These are not real cryptocurrency wallets and do not provide any level of independence, privacy or control over the funds.

The users of these wallets:

- are fully dependent on an exchange to access their funds.
- can lose access to wallets at the sole discretion of an exchange.
- can lose their funds if an exchange gets compromised.
- have very little control over privacy.
- have to go through KYC/AML procedures.