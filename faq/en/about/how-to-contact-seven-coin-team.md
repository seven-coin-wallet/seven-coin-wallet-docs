# How to contact the Seven Coin Team

To reach out, please use sevencoinwallet@gmail.com, or one of the following channels:

Telegram

[https://t.me/+wNYevEGV-lAxMDFl](https://t.me/+wNYevEGV-lAxMDFl)

Do not trust anyone outside official channels. Be aware!

Never share your wallet private keys (aka 12-24 word mnemonic) with anyone, including the Seven Coin team. All potential problems with the wallet app can be addressed without the need for a wallet private key.

When contacting the Seven Coin team regarding a problem with the wallet app, please include:

- App Status logs with the message. Sharing app status logs do not carry any risks and are mainly needed for troubleshooting the problem effectively. The logs are located in **Settings >> About App >> App Status**.

- If the problem relates to a transaction, then also include the public transaction ID, which can be located by clicking on the respective transaction on the Transactions page.

Never share your wallet private keys (aka 12-24 word mnemonic) with anyone!
